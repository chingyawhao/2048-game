// board indicating all numbers on the board
var board;
var highScore;

// function to determine win or lose
var checkWinLose = function() {
  var allOccupied = true;
  var sameTypeNext = false;
  var found2048 = false;
  // check if all spaces are fully occupied
  for(var x = 0; x < 4; x++) {
    for(var y = 0; y < 4; y++) {
      if(board[x][y] == 0)
        allOccupied = false;
      else if(board[x][y] == 2048)
        found2048 = true;
    }
  }
  // check for similar neighbor
  for(var x = 0; x < 4; x++) {
    for(var y = 0; y < 4; y++) {
      if(y > 0 && board[x][y] == board[x][y - 1])
        sameTypeNext = true;
      else if(y < 3 && board[x][y] == board[x][y + 1])
        sameTypeNext = true;
      else if(x < 3 && board[x][y] == board[x + 1][y])
        sameTypeNext = true;
      else if(x > 0 && board[x][y] == board[x - 1][y])
        sameTypeNext = true;
    }
  }
  // return true only if empty space if found to avoid infinity loop looking for an empty space
  if(found2048) {
    var gameboardoverlay = $("#gameboardoverlay");
    var gamewordoverlay = $("#gamewordoverlay");
    gamewordoverlay.html("You won!!!");
    gameboardoverlay.focus();
    gameboardoverlay.addClass("active");
    gamewordoverlay.focus();
    gamewordoverlay.addClass("active");
    gamewordoverlay.addClass("wincolor");
    return false;
  }
  else if(allOccupied && !sameTypeNext) {
    var gameboardoverlay = $("#gameboardoverlay");
    var gamewordoverlay = $("#gamewordoverlay");
    gamewordoverlay.html("You lose...");
    gameboardoverlay.focus();
    gameboardoverlay.addClass("active");
    gamewordoverlay.focus();
    gamewordoverlay.addClass("active");
    gamewordoverlay.addClass("losecolor");
    return false;
  }
  else if(allOccupied)
    return false;
  else
    return true;
};

// function to add a piece to randomly an unoccupied cell
var addRandomPiece = function(score) {
  var scoreBoard = $("#gamescorenotice");
  scoreBoard.html(score.toString());

  if(checkWinLose()) {
    var occupied = true;
    // loop until an empty space is found
    while(occupied) {
      // random x and y coordinate
      var randomX = Math.floor(Math.random() * 4);
      var randomY = Math.floor(Math.random() * 4);
      if(board[randomX][randomY] == 0) {
        // 1 out of 10 chance to get a number 4
        if(Math.floor(Math.random() * 10) < 9)
          var randomResult = 2;
        else
          var randomResult = 4;
        board[randomX][randomY] = randomResult;
        occupied = false;
      }
    }
    var newPiece = $("<div class='gamepiece position" + (randomX + 1) + "-" + (randomY + 1) + "'></div>");
    var corePiece = $("<div class='gamepiececore entering color-" + (randomResult / 2) + "'>" + randomResult + "</div>");
    $("#gameboard").append(newPiece);
    newPiece.append(corePiece);
    setTimeout(function() {
      corePiece.focus();
      corePiece.removeClass("entering");
    }, 300);
  }
};

// function to translate a piece from a location to another and replace its html value
var movePiece = function(fromX, fromY, toX, toY, replaceValue) {
  var toPiece = $("div.position" + (toX + 1) + "-" + (toY + 1));
  var fromPiece = $("div.position" + (fromX + 1) + "-" + (fromY + 1));
  toPiece.css("z-index", "0");
  fromPiece.css("z-index", "5");
  fromPiece.focus();
  fromPiece.removeClass("position" + (fromX + 1) + "-" + (fromY + 1)).addClass("position" + (toX + 1) + "-" + (toY + 1));
  // replace color of div
  switch(replaceValue)
  {
  case 4:
    fromPiece.find("div").removeClass("color-1").addClass("color-2");
    break;
  case 8:
    fromPiece.find("div").removeClass("color-2").addClass("color-3");
    break;
  case 16:
    fromPiece.find("div").removeClass("color-3").addClass("color-4");
    break;
  case 32:
    fromPiece.find("div").removeClass("color-4").addClass("color-5");
    break;
  case 64:
    fromPiece.find("div").removeClass("color-5").addClass("color-6");
    break;
  case 128:
    fromPiece.find("div").removeClass("color-6").addClass("color-7");
    break;
  case 256:
    fromPiece.find("div").removeClass("color-7").addClass("color-8");
    break;
  case 512:
    fromPiece.find("div").removeClass("color-8").addClass("color-9");
    break;
  case 1024:
    fromPiece.find("div").removeClass("color-9").addClass("color-10");
    break;
  case 2048:
    fromPiece.find("div").removeClass("color-10").addClass("color-11");
    break;
  }
  // replace div html value
  fromPiece.find("div").html(replaceValue.toString());
  toPiece.remove();
};
var movePieceUp = function(score) {
  var rearranged = false;
  var spareRow = [5,5,5,5];
  // start from top row
  for(var x = 0; x < 4; x++) {
    for(var y = 0; y < 4; y++) {
      // when a piece is found
      if(board[x][y] != 0) {
        if(spareRow[y] == 5 || board[x][y] != board[spareRow[y]][y])
          spareRow[y] = x;
        else {
          board[spareRow[y]][y] += board[x][y];
          board[x][y] = 0;
          movePiece(x, y, spareRow[y], y, board[spareRow[y]][y]);
          score += board[spareRow[y]][y];
          rearranged = true;
          spareRow[y] = 5;
        }
      }
    }
  }
  // push all board piece to the top
  spareRow = [0,0,0,0];
  for(var x = 0; x < 4; x++) {
    for(var y = 0; y < 4; y++) {
      if(board[x][y] != 0) {
        if(spareRow[y] != x) {
          board[spareRow[y]][y] = board[x][y];
          board[x][y] = 0;
          movePiece(x, y, spareRow[y], y, board[spareRow[y]][y]);
          rearranged = true;
        }
        spareRow[y]++;
      }
    }
  }
  if(rearranged) {
    addRandomPiece(score);
  }
  else
    checkWinLose();
  return score;
};
var movePieceDown = function(score) {
  var rearranged = false;
  var spareRow = [5,5,5,5];
  // start from bottom row
  for(var x = 3; x >= 0; x--) {
    for(var y = 0; y < 4; y++) {
      // when a piece is found
      if(board[x][y] != 0) {
        if(spareRow[y] == 5 || board[x][y] != board[spareRow[y]][y])
          spareRow[y] = x;
        else {
          board[spareRow[y]][y] += board[x][y];
          board[x][y] = 0;
          movePiece(x, y, spareRow[y], y, board[spareRow[y]][y]);
          score += board[spareRow[y]][y];
          rearranged = true;
          spareRow[y] = 5;
        }
      }
    }
  }
  // push all board piece to the bottom
  spareRow = [3,3,3,3];
  for(var x = 3; x >= 0; x--) {
    for(var y = 0; y < 4; y++) {
      if(board[x][y] != 0) {
        if(spareRow[y] != x) {
          board[spareRow[y]][y] = board[x][y];
          board[x][y] = 0;
          movePiece(x, y, spareRow[y], y, board[spareRow[y]][y]);
          rearranged = true;
        }
        spareRow[y]--;
      }
    }
  }
  if(rearranged) {
    addRandomPiece(score);
  }
  else
    checkWinLose();
  return score;
};
var movePieceRight = function(score) {
  var rearranged = false;
  var spareRow = [5,5,5,5];
  // start from right column
  for(var y = 3; y >= 0; y--) {
    for(var x = 0; x < 4; x++) {
      // when a piece is found
      if(board[x][y] != 0) {
        if(spareRow[x] == 5 || board[x][y] != board[x][spareRow[x]])
          spareRow[x] = y;
        else {
          board[x][spareRow[x]] += board[x][y];
          board[x][y] = 0;
          movePiece(x, y, x, spareRow[x], board[x][spareRow[x]]);
          score += board[x][spareRow[x]];
          rearranged = true;
          spareRow[x] = 5;
        }
      }
    }
  }
  // push all board piece to the right
  spareRow = [3,3,3,3];
  for(var y = 3; y >= 0; y--) {
    for(var x = 0; x < 4; x++) {
      if(board[x][y] != 0) {
        if(spareRow[x] != y) {
          board[x][spareRow[x]] = board[x][y];
          board[x][y] = 0;
          movePiece(x, y, x, spareRow[x], board[x][spareRow[x]]);
          rearranged = true;
        }
        spareRow[x]--;
      }
    }
  }
  if(rearranged) {
    addRandomPiece(score);
  }
  else
    checkWinLose();
  return score;
};
var movePieceLeft = function(score) {
  var rearranged = false;
  var spareRow = [5,5,5,5];
  // start from left column
  for(var y = 0; y < 4; y++) {
    for(var x = 0; x < 4; x++) {
      // when a piece is found
      if(board[x][y] != 0) {
        if(spareRow[x] == 5 || board[x][y] != board[x][spareRow[x]])
          spareRow[x] = y;
        else {
          board[x][spareRow[x]] += board[x][y];
          board[x][y] = 0;
          movePiece(x, y, x, spareRow[x], board[x][spareRow[x]]);
          score += board[x][spareRow[x]];
          rearranged = true;
          spareRow[x] = 5;
        }
      }
    }
  }
  // push all board piece to the left
  spareRow = [0,0,0,0];
  for(var y = 0; y < 4; y++) {
    for(var x = 0; x < 4; x++) {
      if(board[x][y] != 0) {
        if(spareRow[x] != y) {
          board[x][spareRow[x]] = board[x][y];
          board[x][y] = 0;
          movePiece(x, y, x, spareRow[x], board[x][spareRow[x]]);
          rearranged = true;
        }
        spareRow[x]++;
      }
    }
  }
  if(rearranged) {
    addRandomPiece(score);
  }
  else
    checkWinLose();
  return score;
};

// high score cookie functions
var setScoreCookie = function(cookieValue) {
  var d = new Date();
  d.setTime(d.getTime() + (3 * 24 * 60 * 60 * 1000));
  var expires = "expires=" + d.toGMTString();
  document.cookie = "2048highscore=" + cookieValue + "; " + expires;
};
var getScoreCookie = function() {
  var name = "2048highscore=";
  var cookieSplit = document.cookie.split(';');
  for(var i = 0; i < cookieSplit.length; i++) {
    var currentCookie = cookieSplit[i];
    while(currentCookie.charAt(0) == ' ') {
      currentCookie = currentCookie.substring(1);
    }
    if(currentCookie.indexOf(name) == 0) {
      return parseInt(currentCookie.substring(name.length, currentCookie.length));
    }
  }
  return 0;
};

$(document).ready(function() {
  // initializing score
  var score;

  highScore = getScoreCookie();

  var initiateNewGame = function() {
    board = [
      [0, 0, 0, 0],
      [0, 0, 0, 0],
      [0, 0, 0, 0],
      [0, 0, 0, 0]
    ];
    score = 0;
    var gameboardoverlay = $("#gameboardoverlay");
    var gamewordoverlay = $("#gamewordoverlay");
    gamewordoverlay.html("");
    gameboardoverlay.focus();
    gameboardoverlay.removeClass("active");
    gamewordoverlay.focus();
    gamewordoverlay.removeClass("active");
    gamewordoverlay.removeClass("wincolor");
    gamewordoverlay.removeClass("losecolor");
    $("div.gamepiece").remove();
    // random twice to display 2 numbers at the beginning
    for(var i = 0; i < 2; i++) {
      addRandomPiece(score);
    }
  };

  initiateNewGame();
  $("#highscorenotice").html(highScore.toString());

  // common check for highscore function
  var checkHighScore = function() {
    if(score > highScore) {
      highScore = score;
      setScoreCookie(highScore);
      var highScoreBoard = $("#highscorenotice");
      highScoreBoard.html(highScore.toString());
    }
  };

  // bind key events
  $("body").keyup(function(event){
    switch(event.which) {
    case 37:
      score = movePieceLeft(score);
      break;
    case 38:
      score = movePieceUp(score);
      break;
    case 39:
      score = movePieceRight(score);
      break;
    case 40:
      score = movePieceDown(score);
      break;
    }
    checkHighScore();
  });

  $("#gameboard").swipe({
    //Generic swipe handler for all directions
    swipe: function(event, direction, distance, duration, fingerCount, fingerData) {
      switch(direction) {
      case "left":
        score = movePieceLeft(score);
        break;
      case "up":
        score = movePieceUp(score);
        break;
      case "right":
        score = movePieceRight(score);
        break;
      case "down":
        score = movePieceDown(score);
        break;
      }
      checkHighScore();
    },
    // 50px distance triggers swipe
    threshold: 50
  });

  $("#newgame").click(function() {
    initiateNewGame();
  });
});
